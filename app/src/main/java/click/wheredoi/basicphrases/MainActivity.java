package click.wheredoi.basicphrases;

import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    public void playSpeech(View view) {
        if (view instanceof Button) {
            MediaPlayer mp = MediaPlayer.create(this, getResources().getIdentifier(
                    view.getTag().toString(), "raw", getPackageName()
            ));
            mp.start();
            Log.v("tag", "The button was clicking and the file should be playing now");
        } else {
            Log.v("tag", "The (clicked) view is not a button");
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Hack to have the newline respected. In XML it's ignored
        //((Button) findViewById(R.id.button)).setText("Do you\nspeak English?");
    }
}
